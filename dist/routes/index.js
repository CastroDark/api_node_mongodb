"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const index_controller_1 = require("../Controllers/index.controller");
const router = express_1.default();
let Controllerw = new index_controller_1.Controller;
router.route("/demo").
    get(Controllerw.showAll);
router.route("/demo/:id").
    get(Controllerw.removeOne);
//get("/",Controllerw.showAll);
exports.default = router;
