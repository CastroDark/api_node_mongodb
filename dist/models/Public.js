"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const safePasswords = new mongoose_1.Schema({
    title: String,
    userName: String,
    dateTime: String,
    password: String,
    note: String
});
exports.default = mongoose_1.model("SafePasswords", safePasswords);
