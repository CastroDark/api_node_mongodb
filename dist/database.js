"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
async function startConnect() {
    await mongoose_1.default.connect("mongodb://admin:12345@172.16.18.121:5500/db_documents", {
        useNewUrlParser: true
    });
    console.log("Database is Connected!!");
}
exports.startConnect = startConnect;
