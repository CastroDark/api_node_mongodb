import jwt from "jwt-simple";
import moment from "moment";
import { DbContext } from "../models/index";
const uuidv1 = require("uuid/v1");
import { Setting } from "../config";
import { AuthenticateService } from "../Services/index";
import { IPayloadToken, IUserLogin, IUser, ICompany } from "../Interfaces/index";


const Bcrypt = require("bcryptjs");

const db = new DbContext();

export class FuncionsService {
  constructor() { }

  //#region Utils

  async addEventApi(
    level: number,
    event: string,
    idCompany: string
  ): Promise<boolean> {
    const result = new Promise<boolean>((resolve, reject) => {
      db.addEventsApi(level, event, idCompany).then(data => {
        resolve(data);
        //console.log(data);
      });
    });
    return result;
  }

  async addEventLogUser(
    level: number,
    event: string,
    idUser: string
  ): Promise<boolean> {
    const result = new Promise<boolean>((resolve, reject) => {
      db.addEventsUser(level, event, idUser).then(data => {
        resolve(data);
      });
    });
    return result;
  }

  async GenerateUuid(): Promise<string> {
    const result = new Promise<string>((resolve, reject) => {
      const uuid = uuidv1();
      resolve(uuid);
    });
    return result;
  }

  async GetUserLoginToken(token: string): Promise<IUser> {
    const result = new Promise<IUser>(async (resolve, rejects) => {
      const payload: IPayloadToken = await jwt.decode(token, Setting.SECRET, true);
      await db.GetUserLoginToken(payload.idSession).then(data => {
        resolve(data);
      });
    });

    return result;
  }

  async GetProfileCompany(idCompany: string): Promise<ICompany> {
    const result = new Promise<ICompany>(async (resolve, rejects) => {
      db.GetProfileCompany(idCompany).then(profile => {
       
        resolve(profile);
      });
    });
    return result;
  }




  //#endregion
}
