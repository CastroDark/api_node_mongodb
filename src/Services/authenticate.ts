import jwt from "jwt-simple";
import moment from "moment";
import { DbContext } from "../models/index";

import { Setting } from "../config";
import { IPayloadToken, IUserLogin, IUser,ISendToken } from "../Interfaces/index";
import { IUserDocument } from '../Interfaces/DbInterfaces';


const Bcrypt = require("bcryptjs");

const db = new DbContext();
export class AuthenticateService {
  constructor() {}

  //#region Authorization User

  async checkUserLogin(userLogin: IUserLogin): Promise<IUser> {
    const result = new Promise<IUser>((resolve, reject) => {
      db.loginUser(userLogin).then(async data => {       
        if (data) {
          //check Password hash is true
          const resultHash = await Bcrypt.compare(
            userLogin.password,data.password
          );
          if (resultHash) {
            resolve(data);
          } else {
            resolve();
          }
        } else {         
          resolve();
        }
      }
      );
    });
    return result;
  }

  async buildToken(payload: IPayloadToken) {
    const SECRET_TOKEN = Setting.SECRET;
    const token = await jwt.encode(payload, SECRET_TOKEN, "HS512");
    return token;
  }

  async checkStatusTOken(payload: IPayloadToken) {
    let statusToken = 0;
    if (moment(payload.exp).toDate() < moment().toDate()) {
      statusToken = 0;
    } else {
      statusToken = 1;
    }
  //#here is use functions to check status token in JWT SESSION DB
    return statusToken;
  }

  async AddUser(newUser: IUser): Promise<any> {
    const result = new Promise<any>((resolve, reject) => {
      db.addNewUser(newUser).then(data => {
        if (data) {
          resolve(data);
        } else {
          resolve(data);
        }
      });
    });
    return result;
  }

  async AddSessionJwt(idSession:string,user:IUser,token:string,remember:boolean): Promise<ISendToken> {
    const result = new Promise<ISendToken>((resolve, reject) => {     
      db.assignJwtSession(idSession,user,token,remember).then(data=>{       
        resolve(data);
       // console.log(data);
      });
    });
    return result;
  }





  //#endregion

}
