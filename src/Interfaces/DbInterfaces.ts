import { Schema, Document, model, Model } from "mongoose";

//@@@@ Users
export interface IUserDocument extends Document {

  name: string;
  nickName: string;
  password: string;
  note: string;
  idRol: number;
  [status: number]: any;
  [lastLogin: string]: any;
  idCompany: string;
}

export interface IUser {

  name: string;
  nickName: string;
  password: string;
  [status: number]: any;
  note: string;
  idRol: number;
  [lastLogin: string]: any;
  idCompany: string;
}
///####@@@@@@@@@

//@@@@@@ JWT Tokens
export interface IJwtDocument extends Document {
  name: string;
  nickName: string;
  password: string;
  status: number;
  note: string;
  idRol: number;
  [lastLogin: string]: any;
}

export interface IJWT {
  name: string;
  nickName: string;
  password: string;
  status: number;
  note: string;
  idRol: number;
  [lastLogin: string]: any;
}
///@@@@@@@@@@@

//@@@@ Users
export interface IEventApiDocument extends Document {
  level: number;
  event: string;
  [date: string]: any;
  [idCompany: string]: any;
  [status: string]: any;
}

export interface IEventApi {
  level: number;
  event: string;
  [date: string]: any;
  [idCompany: string]: any;
  [status: string]: any;
}
///####@@@@@@@@@



//@@@@ Log User
export interface IEventUserDocument extends Document {
  level: number;
  event: string;
  idUser: string;
  [date: string]: any;
  [idCompany: string]: any;
  [status: string]: any;
}

export interface IEventUser {
  level: number;
  event: string;
  idUser: string;
  [date: string]: any;
  [idCompany: string]: any;
  [status: string]: any;
}
///####@@@@@@@@@


//@@@@ Session Jwt
export interface ISessionJwtDocument extends Document {
  idSession: string;
  date: Date;
  idUser: string;
  token: string;
  status: number;
  persistent: boolean;
}

export interface ISessionJwt {
  idSession: string;
  date: Date;
  idUser: string;
  token: string;
  status: number;
  persistent: boolean;
}
///####@@@@@@@@@




//@@@@ Companys
export interface ICompanyDocument extends Document {
  idCompany: string;
  shortName: string;
  longName: string;
  slogan: string;
  address: string;
  contact: string;
  others: string;
  appPhoneVers:string;
  appDesktopVers:string;
  pathDownloadApp:string;
  pathCloudDownload:string;
  status:number;
}

export interface ICompany {
  idCompany: string;
  shortName: string;
  longName: string;
  slogan: string;
  address: string;
  contact: string;
  others: string;
  appPhoneVers:string;
  appDesktopVers:string;
  pathDownloadApp:string;
  pathCloudDownload:string;
  status:number;
}
///####@@@@@@@@@

