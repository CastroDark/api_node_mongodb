import { Interface } from "readline";

export interface IPayloadToken {
  idSession: string;
  ait: Date;
  exp: Date;
}

export interface IUserLogin {
  userName: string;
  password: string;
  remember: string;
}

export interface ISendToken {
  token: string;
  expire: Date;
  status: number;
}
