
import { Request, Response, NextFunction } from "express";

import { DbContext } from "../models/index";
import { IUser, IPayloadToken, ISendToken } from "../Interfaces/index";
import { AuthenticateService, FuncionsService } from "../Services/index";
import moment from "moment";
import { json } from "body-parser";
import { IUserLogin } from "../Interfaces/index";

import path from 'path';
import { token } from "morgan";
import { Db } from "mongodb";
import { ICompany } from '../Interfaces/DbInterfaces';


const contextDb = new DbContext();
const authService = new AuthenticateService();
const funcService = new FuncionsService();
let locaUser: IUser;
let localCompany: ICompany;
export class AuthorizateController {

  constructor() {
    //const token = req.headers.authorization.split(" ")[1];
  }

  async loginUser(req: Request, res: Response): Promise<Response> {
    const ipRemote = req.connection.remoteAddress

    const reqUser: IUserLogin = {
      userName: req.body.userName,
      password: req.body.password,
      remember: req.body.remember
    };
    var newToken: ISendToken = {
      expire: new Date(),
      status: 1,
      token: ""
    };
    await authService.checkUserLogin(reqUser).then(async profileUser => {
      if (profileUser) {
        let expire = reqUser.remember === "true" || reqUser.remember === "True" ? moment().add(2, "days").toDate() : moment().add(30, "minutes").toDate()
        let idSession = await funcService.GenerateUuid();

        const payload: IPayloadToken = {
          idSession: idSession,
          ait: moment().toDate(),
          exp: expire
        };

        let persistent = (reqUser.remember === "true" || reqUser.remember === "True") ? true : false;
        await authService.buildToken(payload).then(async dataToken => {
          await authService.AddSessionJwt(idSession, profileUser, dataToken, persistent).then(data => {
            newToken.expire = expire;
            newToken.token = data.token;
            newToken.status = data.status;
          });
        });

        if (newToken.token) {
          //add Event log User login 
          funcService.addEventLogUser(1, "Inicio Session..", profileUser._id);
        }

      } else {
        //user is not valid
        console.log("Intent login Fails.. == >", profileUser);
        newToken.status = 0;
        funcService.addEventApi(3, `Intento Login Fallido..: ${reqUser.userName}, "Desde : " ${ipRemote}`, "");

      }
    });
    return res.json(newToken);
  }

  async addUser(req: Request, res: Response): Promise<Response> {

    //get UserOnline To Token...
    if (req.headers.authorization) {
      const token = req.headers.authorization.split(" ")[1];
      await funcService.GetUserLoginToken(token).then(data => {
        if (data) {
          locaUser = data;
          //console.log("==> ", data);
        }
      });
    };

    //get profile company 
    if (locaUser) {
      await funcService.GetProfileCompany(locaUser.idCompany).then(profile => {
        //console.log(profile);
      });
    }


    const newUser: IUser = {
      idRol: 11111,
      name: "Eduardo Castro",
      nickName: "Castro".toLowerCase(),
      note: "Nota Del Usuario",
      status: 1,
      idCompany: "888888",
      password: "12345"
    };

    let result;

    //test add event webapi
    funcService.addEventApi(1, "Agregando Usuario Para ver el Evento", "").then(data => {
      
      // console.log(data);
      });

    // //test add user
    // await authService.AddUser(newUser).then(data => {
    //   // console.log(" si => ", data);
    //   result = data;
    // });

    if (result) {
      return res.json({ result: "Usuario creado" });
    } else {
      return res.json({ result: "Error Al Crear Usuario" });
    };
  }

  async showAll(req: Request, res: Response): Promise<Response> {
    return res.json({});
  }

  async removeOne(req: Request, res: Response): Promise<Response> {
    //const { id } = req.params;

    // const resul = await DATA.findByIdAndRemove(id, (error, ok) => {

    //   if (error) {
    //     console.log(error);
    //   } else {
    //     console.log(ok);
    //   }
    // });

    return res.json({ result: "Delete" });
  }

  async getProfileUserOnline(req: Request, res: Response): Promise<Response> {
    let sendProfile: any;
    //get UserOnline To Token...
    if (req.headers.authorization) {
      const token = req.headers.authorization.split(" ")[1];
      await funcService.GetUserLoginToken(token).then(profile => {
        if (profile) {
          //clean password no den to client
          profile.password = "";
          sendProfile = profile;

        }
      });
    };
    return res.json(sendProfile);

  }

  async getProfileCompany(req: Request, res: Response): Promise<Response> {
    let sendProfile: any;
    //get UserOnline To Token...
    if (req.headers.authorization) {
      const token = req.headers.authorization.split(" ")[1];
      await funcService.GetUserLoginToken(token).then(async profile => {
        if (profile) {
          
          //get profile company          
          await funcService.GetProfileCompany(profile.idCompany).then(profile => {
            sendProfile = profile;
            //console.log(sendProfile)
          });
        }
      });
    };

    return res.json(sendProfile);

  }




  // async addUser(req: Request, res: Response): Promise<Response> {
  //   //      const data = DATA.find()
  //   //  const resul = await data;
  //   //Tests Create New User to Db
  //   //   let newUser: IUser = {
  //   //     name: "demo",
  //   //     nickName: "demo",
  //   //     password: "demo",
  //   //     status: 1,
  //   //     note: "demo",
  //   //     idRol: 12
  //   //   };
  //   //  let resul= await contextDb.demoConsulta(newUser);
  //   //  console.log(resul);

  //   //this.buildToken();

  //   let expire: number;
  //   expire = moment()
  //     .add(10, "minutes")
  //     .unix();

  //   const payload: IPayloadToken = {
  //     idSession: "778123123213213",
  //     ait: moment().unix(),
  //     exp: expire
  //   };

  //   let token = await authService.buildToken(payload);

  //   return res.json({ token: token });
  // }
}
