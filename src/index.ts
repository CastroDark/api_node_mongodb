import app from "./app";
import { startConnect } from "./database";

async function main() {
  //connect to db 
 // startConnect();
  
  await app.listen(app.get("port"), () => {
    console.log(`Listeners in Port: ${app.get("port")}`);
  });
}

main();
