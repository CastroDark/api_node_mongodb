import { Schema, Document, model, Model } from "mongoose";
import { IEventApiDocument } from "../Interfaces/index";


const nameCollection = "EventsApi";

const thisSchema = new Schema({
  level: Number,
  event: String,
  date: { type: Date, default: Date.now() },
  idCompany: { type: String, default: "" },
  status: { type: Number, default: 1 }
});

// //Test Hash Password to user in Db
// let user: any = thisSchema;
// thisSchema.pre("save", async function(next) {
//   if (!this.isModified("password")) {
//     return next();
//   }
//   try {
//     //const hash = await Bcrypt("admin", 16.5);
//     //const paass= hash;
//     var docu: any = this;
//     docu.password = Bcrypt.hashSync(docu.password, 12);

//     //compare password
//     //const resul= await Bcrypt.compare("admin",docu.password)
//    // console.log(resul);
//     next();
//   } catch (err) {
//     next(err);
//   }
// });

export default model<IEventApiDocument>(nameCollection, thisSchema);
