import { Schema, Document, model, Model } from "mongoose";
import {IUserDocument} from '../Interfaces/index'

const Bcrypt = require("bcryptjs");
const SALT_WORK_FACTOR = 10;

const nameCollection="Users";

const thisSchema = new Schema({

  name: String,
  nickName: String,
  password: String,
  status:{ type: Number, default: 0},
  note: String,
  idRol: Number,
  lastLogin:  { type: Date, default: Date.now() },
  idCompany:String
  // title:String;
  // userName: string;
  // dateTime: string;
  // password: string;
  // note: string;
  // // title: String,
  // userName: String,
  // dateTime: String,
  // password: { type: String, required: true },
  // note: { type: Date, default: Date.now() }
});

//Test Hash Password to user in Db
let user: any = thisSchema;
thisSchema.pre("save", async function(next) {
  if (!this.isModified("password")) {
    return next();
  }
  try {
    //const hash = await Bcrypt("admin", 16.5);
    //const paass= hash;
    var docu: any = this;
    docu.password = Bcrypt.hashSync(docu.password, 12);
          
    //compare password
    //const resul= await Bcrypt.compare("admin",docu.password)
   // console.log(resul);
    next();
  } catch (err) {
    next(err);
  }
});


export default model<IUserDocument>(nameCollection, thisSchema);
