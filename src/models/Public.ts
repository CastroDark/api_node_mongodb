import { Schema, Document, model, Model } from "mongoose";
const Bcrypt = require("bcryptjs");
const SALT_WORK_FACTOR = 10;

interface ISafePasswords extends Document {
  title: string;
  userName: string;
  dateTime: string;
  password: string;
  note: string;
}

const safePasswords = new Schema({
  title: String,
  userName: String,
  dateTime: String,
  password: { type: String, required: true },
  note: { type: Date, default: Date.now() }
});

// bcrypt = Promise.promisifyAll(require("bcrypt"));

let user: any = safePasswords;

safePasswords.pre("save", async function(next) {
  if (!this.isModified("password")) {
    return next();
  }
  try {
    //const hash = await Bcrypt("admin", 16.5);
    //const paass= hash;
    var docu: any = this;
    docu.password = Bcrypt.hashSync(docu.password, 12);
      
    
    //compare password
    const resul= await Bcrypt.compare("admin",docu.password)
    console.log(resul);
    next();
  } catch (err) {
    next(err);
  }
});


export default model<ISafePasswords>("SafePasswords", safePasswords);
