import Tbuser from "./User.Collections";
import TbEventsApi from "./LogEventsApi.Collections";
import TbSessionJwt from "./SessionJwtCollection";
import TbEventsUser from './LogEventUser.Collections'
import TbCompany from './Companys.Collection'
import { ICompany } from '../Interfaces/DbInterfaces';


import {
  IPayloadToken,
  ISendToken, IUser,
  IUserLogin, IEventApi,
  ISessionJwt, IEventUser
} from "../Interfaces/index";

export class DbContext {
  constructor() { }
  //#region Authorized
  async demoConsulta(user: IUser): Promise<any> {
    const result = new Promise<any>(async (resolve, rejects) => {
      //var result = await Tbuser.find();
      const tbUser = new Tbuser(user);
      await tbUser.save((error, ok) => {
        if (error) {
          resolve(error);
        } else {
          resolve(ok);
        }
      });
    });

    return result;

    //console.log(result);
  }

  async loginUser(userLogin: IUserLogin): Promise<IUser> {
    const result = new Promise<IUser>(async (resolve, rejects) => {
      //var result = await Tbuser.find();
      //const userName= new RegExp('^${userLogin.userName}$/i')
      //  let query = {nickName:userName}
      //IGNORE UPPERCASE MONGODB
      const userFound = await Tbuser.find({ nickName: userLogin.userName.toLowerCase() });
      if (userFound.length) {
        const result2 = await Tbuser.findById({ _id: userFound[0]._id });
        //console.log(result2);
        if (result2) {
          resolve(result2);
        } else {
          resolve();
        }
      }
      resolve();
    });
    return result;
  }

  async addNewUser(dataUser: IUser): Promise<IUser> {
    const result = new Promise<IUser>(async (resolve, rejects) => {
      //var result = await Tbuser.find();
      const query = new Tbuser(dataUser);
      const result = await query.save();
      if (result) {
        resolve(result);
      } else {
        resolve();
      }
    });

    return result;
  }

  async assignJwtSession(idSession: string,
    user: IUser, token: string, remember: boolean
  ): Promise<ISendToken> {
    const result = new Promise<ISendToken>(async (resolve, rejects) => {
      let sendToken: ISendToken = {
        status: 1,
        token: token,
        expire: new Date()
      }


      //const idSession = uuidv1(); 
      let idUser = user._id

      const exist = await TbSessionJwt.find({ idUser: idUser });

      //console.log(user._id);
      var newJws: ISessionJwt = {
        idSession: idSession,
        idUser: idUser,
        date: new Date(),
        token: token,
        status: 1,
        persistent: remember
      };

      if (exist.length) {
        //console.log(exist[0]._id);
        var query = new TbSessionJwt(newJws);
        const update = await TbSessionJwt.updateOne({ _id: exist[0]._id }, newJws);
        resolve(sendToken);
      } else {

        var query = new TbSessionJwt(newJws);
        if (query.save()) {
          resolve(sendToken);
        } else {
          resolve();
        }
      }

    });
    return result;
  }


  //#region ManagedProfiles 
  async GetUserLoginToken(idSession: string): Promise<IUser> {
    const result = new Promise<IUser>(async (resolve, rejects) => {
      const sessionToken = await TbSessionJwt.findOne({ idSession: idSession });
      if (sessionToken) {
        const userOnline = await Tbuser.findOne({ _id: sessionToken.idUser });
        if (userOnline) {
          resolve(userOnline);
        };
      }
      resolve();
    });
    return result;
  }

  async GetProfileCompany(idCompany: string): Promise<ICompany> {
    const result = new Promise<ICompany>(async (resolve, rejects) => {
      const profile = await TbCompany.findOne({ idCompany: idCompany });      
      if (profile) {        
        resolve(profile);
      }
      resolve();
    });
    return result;
  }

  //#endregion






  //#endregion

  //#region Events api and events logs User
  async addEventsApi(
    level: number,
    event: string,
    idCompany: string
  ): Promise<boolean> {
    const result = new Promise<boolean>(async (resolve, rejects) => {
      let newEvent: IEventApi = {
        event: event,
        level: level,
        idCompany: idCompany
      };
      const query = new TbEventsApi(newEvent);
      if (query.save()) {
        resolve(true);
      } else {
        resolve(false);
      }
    });

    return result;

    //console.log(result);
  }

  async addEventsUser(
    level: number,
    event: string,
    idUser: string
  ): Promise<boolean> {
    const result = new Promise<boolean>(async (resolve, rejects) => {
      let newEvent: IEventUser = {
        event: event,
        idUser: idUser,
        level: level,
      };
      const query = new TbEventsUser(newEvent);
      if (query.save()) {
        resolve(true);
      } else {
        resolve(false);
      }
    });

    return result;

    //console.log(result);
  }

  //#endregion

  //#region "Save User"

  //#endregion
}
