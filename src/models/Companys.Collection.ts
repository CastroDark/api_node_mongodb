import { Schema, Document, model, Model } from "mongoose";
import { ICompanyDocument } from "../Interfaces/index";

const nameCollection = "companys";

const thisSchema = new Schema({
  idCompany: String,
  shortName: String,
  longName: String,
  slogan: String,
  address: String,
  contact: String,
  others: String,
  appPhoneVers:String,
  appDesktopVers:String,
  pathDownloadApp:String,
  pathCloudDownload:String,
  status:Number  
});

export default model<ICompanyDocument>(nameCollection, thisSchema);
