import { Schema, Document, model, Model } from "mongoose";
import { IEventUserDocument } from "../Interfaces/index";


const nameCollection = "EventsUser";

const thisSchema = new Schema({
  level: Number,
  event: String,
  idUser: String,
  date: { type: Date, default: Date.now() },
  idCompany: { type: String, default: "" },
  status: { type: Number, default: 1 }
});

export default model<IEventUserDocument>(nameCollection, thisSchema);
