import { Schema, Document, model, Model } from "mongoose";
import { ISessionJwtDocument } from "../Interfaces/index";

const nameCollection = "sessionjwt";

const thisSchema = new Schema({
  idSession:String,
  date:Date,
  idUser:String,
  token:String,
  status:Number,
  persistent:Boolean, 
});

export default model<ISessionJwtDocument>(nameCollection, thisSchema);
