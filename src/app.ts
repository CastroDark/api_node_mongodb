import express from 'express'
import morgan from 'morgan'
import indexRouter from './routes/index'
import authorizateRouter from './routes/authorizate'
import bodyParser from 'body-parser'



import path from 'path'
const app = express();

//Setting
app.set('port',process.env.PORT || 4000);
app.set('json spaces', 2);

//middlewares
app.use(morgan("dev"));
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Declare routes
app.use('/api',indexRouter);
app.use('/api',authorizateRouter);



//this folder public will is use for storage public files
app.use('publics',express.static(path.resolve('publics')));



export default  app;


