import Router from "express";
import { AuthorizateController} from '../Controllers/index';

import {Authotizate} from '../middlewares/index'

const authoMiddleware= new Authotizate();

const router = Router();
let Controller = new AuthorizateController();


router.route("/auth").
get( Controller.showAll);

router.route("/demo/:id").
get( Controller.removeOne)

router.route("/auth",).post(authoMiddleware.isAuthorizate,Controller.addUser);
router.route("/auth/loginUser",).post(Controller.loginUser);
router.route("/auth/addUser",).post(Controller.addUser);
router.route("/auth/getProfileUsrOnline",).post(authoMiddleware.isAuthorizate,Controller.getProfileUserOnline);
router.route("/auth/getProfileCompany",).post(authoMiddleware.isAuthorizate,Controller.getProfileCompany);


//get("/",Controllerw.showAll);

export default router;
