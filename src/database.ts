import mongoose from "mongoose";
import { Setting } from './config';
 

export async function startConnect() {


  // const statusConection = await mongoose.connection.readyState;  
  // if (statusConection === 0) {
  // }

    await mongoose.connect(
     Setting.MONGO_CONNECT,
      {
        useNewUrlParser: true
      }
    );
    console.log("Database is Connected!!");
 
}
