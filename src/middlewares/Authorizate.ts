import { Request, Response, NextFunction } from "express";
import jwt from "jwt-simple";

import { Setting } from "../config";

import { AuthenticateService } from "../Services/index";
import { IPayloadToken } from "../Interfaces/Publics";
import moment from "moment";

const authService = new AuthenticateService();

export class Authotizate {
  async isAuthorizate(req: Request, res: Response, next: NextFunction) {
    if (!req.headers.authorization) {
      return res.status(403).send({ message: "not Authorizate.." });
    }
     const token = req.headers.authorization.split(" ")[1];
     const payload: IPayloadToken = await jwt.decode(token, Setting.SECRET,true); 
     

     console.log("Desde Middleware Token Expire :",moment(payload.exp).format("DD/MM/YYYY HH:mm:ss"));
     
     //console.log(token);
     //console.log("==>",payload);
    let statusToken = await authService.checkStatusTOken(payload);
    //console.log(statusToken);
    if(statusToken===0){
      return res.status(403).send({ message: "not Authorizate.." });
    }else{
      next();     
    } 
   
  }
}
